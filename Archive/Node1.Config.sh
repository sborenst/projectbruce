CONF_INSTALL_COMPONENTS="node" # Defines the role of this machine. 
CONF_CARTRIDGES="all,-jobsseap" #This will install all standard cartridges.
CONF_NODE_HOSTNAME="ose-node1.ose-poc.nbnco.internal" # This must be set to the desired machine name for this host. It should already be in DNS.
CONF_BROKER_IP_ADDR="192.168.0.131" #This should be set to the first broker built (ie the one with named). The script will use this IP for DNS resolution as well.

