####################On Node
####################Be root

#################### Variable definition
export NODETYPE=node-eap

export BROKERPUBLICIP=54.66.194.245
export NODE01PUBLICIP=54.66.192.241
export NODE02PUBLICIP=54.66.190.179

export TOPDOMAINNAME=testdrive.tftl.org 
export APPSDOMAINNAME=apps.${TOPDOMAINNAME}
export HOSTDOMAINNAME=hosts.${TOPDOMAINNAME}

export BROKERHOSTNAME=broker01.${HOSTDOMAINNAME}
export NODE01HOSTNAME=node01.${HOSTDOMAINNAME}
export NODE02HOSTNAME=node02.${HOSTDOMAINNAME}


export GEARSIZEOPTIONS=small
export WHATNODEAMI=$NODE02HOSTNAME

export GEARSIZEOPTIONS=small
export GEARSIZEDEFAULT=small

export MCOLLECTPASS=password1
export MONGODBPASS=password1
export DEMOUSERPASS=testdrive1

export CARTRIDGEOPS=all
# cartridges / CONF_CARTRIDGES
#   Comma-separated selections from the following:
#     all - all cartridges;
#     standard - all cartridges except for JBossEWS or JBossEAP;
#     cron - embedded cron support;
#     diy - do-it-yourself cartridge;
#     haproxy - haproxy support for scalable apps;
#     jbossews - JBossEWS support;
#     jobsseap - JBossEAP support;
#     jboss - alias for jbossews and jbosseap;
#     jenkins - Jenkins client and server for continuous integration;
#     mongodb - MongoDB;
#     mysql - MySQL;
#     nodejs - NodeJS;
#     perl - mod_perl support;
#     php - PHP support;
#     postgresql - PostgreSQL support;
#     postgres - alias for postgresql;
#     python - Python support;
#     ruby - Ruby Rack support running on Phusion Passenger.

#################### End Variable definition



subscription-manager repos --enable rhel-6-server-ose-2.1-node-rpms
yum install openshift-enterprise-release -y
oo-admin-yum-validator -o 2.1 -r $NODETYPE --fix-all



## This is a workaround for the multilib bug. should be fixed in the next few days
yum install libcgroup-pam --enablerepo=rhel-6-server-optional-rpms --disableplugin=priorities -y
yum update -y 
curl -O https://raw.githubusercontent.com/openshift/openshift-extras/enterprise-2.1/enterprise/install-scripts/generic/openshift.sh





## The script will run this command 
echo
echo
echo
echo sh openshift.sh install_components=node domain=$APPSDOMAINNAME \
hosts_domain=$HOSTDOMAINNAME node_hostname=$WHATNODEAMI \
named_ip_addr=$BROKERPUBLICIP broker_ip_addr=$BROKERPUBLICIP broker_hostname=$BROKERHOSTNAME  \
node_profile=$GEARSIZEDEFAULT cartridges=$CARTRIDGEOPS \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log


echo this is a debug break, and needs to be removed in production 
#read x 

sh openshift.sh install_components=node domain=$APPSDOMAINNAME \
hosts_domain=$HOSTDOMAINNAME node_hostname=$WHATNODEAMI \
named_ip_addr=$BROKERPUBLICIP broker_ip_addr=$BROKERPUBLICIP broker_hostname=$BROKERHOSTNAME  \
node_profile=$GEARSIZEDEFAULT cartridges=$CARTRIDGEOPS \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log


#watch echo keepalive Completed node Script 

break
exit
exit
break




#openshift-origin-cartridge-cron.noarch : Embedded cron support for OpenShift
#openshift-origin-cartridge-dependencies-optional-jbosseap.noarch : Optional user dependencies for JBossEAP OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-jbossews.noarch : Optional user dependencies for JBossEWS OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-nodejs.noarch : Optional user dependencies for Nodejs OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-perl.noarch : Optional user dependencies for Perl OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-php.noarch : Optional user dependencies for PHP OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-python.noarch : Optional user dependencies for Python OpenShift Cartridges
#openshift-origin-cartridge-dependencies-optional-ruby.noarch : Optional user dependencies for Ruby OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-jbosseap.noarch : Recommended user dependencies for JBossEAP OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-jbossews.noarch : Recommended user dependencies for JBossEWS OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-nodejs.noarch : Recommended user dependencies for Nodejs OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-perl.noarch : Recommended user dependencies for Perl OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-php.noarch : Recommended user dependencies for PHP OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-python.noarch : Recommended user dependencies for Python OpenShift Cartridges
#openshift-origin-cartridge-dependencies-recommended-ruby.noarch : Recommended user dependencies for Ruby OpenShift Cartridges
#openshift-origin-cartridge-diy.noarch : DIY cartridge
#openshift-origin-cartridge-haproxy.noarch : Provides HA Proxy
#openshift-origin-cartridge-jbosseap.noarch : Provides JBossEAP6.0 support
#openshift-origin-cartridge-jbossews.noarch : Provides JBossEWS2.0 support
#openshift-origin-cartridge-jenkins.noarch : Provides jenkins-1.x support
#openshift-origin-cartridge-jenkins-client.noarch : Embedded jenkins client support for OpenShift
#openshift-origin-cartridge-mock.noarch : Mock cartridge for V2 Cartridge SDK
#openshift-origin-cartridge-mock-plugin.noarch : Mock plugin cartridge for V2 Cartridge SDK
#openshift-origin-cartridge-mongodb.noarch : Embedded mongodb support for OpenShift
#openshift-origin-cartridge-mysql.noarch : Provides embedded mysql support
#openshift-origin-cartridge-nodejs.noarch : Provides Node.js support
#openshift-origin-cartridge-perl.noarch : Perl cartridge
#openshift-origin-cartridge-php.noarch : Php cartridge
#openshift-origin-cartridge-postgresql.noarch : Provides embedded PostgreSQL support
#openshift-origin-cartridge-python.noarch : Python cartridge
#openshift-origin-cartridge-ruby.noarch : Ruby cartridge













#not working
yum install -y openshift-origin-cartridge-jbosseap.noarch
yum install -y openshift-origin-cartridge-jbossews.noarch


#TESTED
yum install -y openshift-origin-cartridge-dependencies-recommended-jbosseap.noarch
yum install -y openshift-origin-cartridge-dependencies-recommended-jbossews.noarch
yum install -y openshift-origin-cartridge-dependencies-recommended-nodejs.noarch




yum install -y openshift-origin-cartridge-python.noarch
yum install -y openshift-origin-cartridge-ruby.noarch
yum install -y openshift-origin-cartridge-perl.noarch
yum install -y openshift-origin-cartridge-php.noarch
yum install -y openshift-origin-cartridge-python.noarch
yum install -y openshift-origin-cartridge-ruby.noarch

yum install -y openshift-origin-cartridge-diy.noarch
yum install -y openshift-origin-cartridge-haproxy.noarch

yum install -y openshift-origin-cartridge-jenkins.noarch
yum install -y openshift-origin-cartridge-jenkins-client.noarch
yum install -y openshift-origin-cartridge-mock.noarch
yum install -y openshift-origin-cartridge-mock-plugin.noarch

yum install -y openshift-origin-cartridge-mongodb.noarch
yum install -y openshift-origin-cartridge-postgresql.noarch
yum install -y openshift-origin-cartridge-nodejs.noarch


yum install -y openshift-origin-cartridge-dependencies-recommended-perl.noarch
yum install -y openshift-origin-cartridge-dependencies-recommended-php.noarch
yum install -y openshift-origin-cartridge-dependencies-recommended-python.noarch
yum install -y openshift-origin-cartridge-dependencies-recommended-ruby.noarch

yum install -y openshift-origin-cartridge-cron.noarch 
yum install -y openshift-origin-cartridge-dependencies-optional-nodejs.noarch
yum install -y openshift-origin-cartridge-dependencies-optional-perl.noarch
yum install -y openshift-origin-cartridge-dependencies-optional-php.noarch
yum install -y openshift-origin-cartridge-dependencies-optional-python.noarch
yum install -y openshift-origin-cartridge-dependencies-optional-ruby.noarch

yum install -y openshift-origin-cartridge-dependencies-optional-jbosseap.noarch
yum install -y openshift-origin-cartridge-dependencies-optional-jbossews.noarch






 cd 
 wget 'https://content-web.rhn.redhat.com/rhn/public/NULL/jboss-transaction-api_1.1_spec/1.0.1-12.Final_redhat_2.2.ep6.el6/noarch/jboss-transaction-api_1.1_spec-1.0.1-12.Final_redhat_2.2.ep6.el6.noarch.rpm?__gda__=1413447753_38421c710807788e3a94fd7a321f89f6&ext=.rpm'
 mv jboss-trans* jboss-transaction-api_1.1_spec-1.0.1-12.Final_redhat_2.2.ep6.el6.noarch.rpm 

yum localinstall -y jboss-transaction-api_1.1_spec-1.0.1-12.Final_redhat_2.2.ep6.el6.noarch.rpm 
yum install -y openshift-origin-cartridge-mysql.noarch

#after we install all the cartridges we need to restart the mcollective service
 service ruby193-mcollective restart


## These are commands for the broker to run after 
oo-admin-ctl-cartridge -c import-node --activate --obsolete --node node01.hosts.testdrive.tftl.org #--dry-run
oo-admin-ctl-cartridge -c import-node --activate --obsolete --node node02.hosts.testdrive.tftl.org #--dry-run
oo-admin-ctl-cartridge -c list

oo-admin-ctl-cartridge -c clean

oo-admin-broker-cache --clear # --console
oo-admin-console-cache --clear
 
 # my current error https://github.com/openshift/wordpress-example.git or https://github.com/openshift/wordpress-example 
#   The initial build for the application failed: Shell command '/sbin/runuser -s /bin/sh 543f899b97f2219a8100008e -c "exec /usr/bin/runcon 'unconfined_u:system_r:openshift_t:s0:c3,c570' /bin/sh -c \"gear postreceive --init >> /tmp/initial-build.log 2>&1\""' returned an error. rc=255 .Last 10 kB of build output: Stopping PHP 5.4 cartridge (Apache+mod_php) Repairing links for 1 deployments Building git ref 'master', commit 119757b Checking .openshift/pear.txt for PEAR dependency... Preparing build for deployment Deployment id is aedc395e Activating deployment Copying WordPress plugins from .openshift/plugins Copying WordPress themes from .openshift/themes Copying WordPress languages from .openshift/languages Could not find mysql database. Please run: rhc cartridge add -a tete2w -c mysql-5.1 then make a sample commit (add whitespace somewhere) and re-push ------------------------- Git Post-Receive Result: failure Activation status: failure Activation failed for the following gears: 543f899b97f2219a8100008e (Error activating gear: CLIENT_ERROR: Failed to execute action hook 'deploy' for 543f899b97f2219a8100008e application tete2w) Deployment completed with status: failure postreceive failed 
#Based On
#PHP 5.4 Cartridge

