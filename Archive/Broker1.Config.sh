CONF_INSTALL_COMPONENTS="broker,named,activemq,datastore" # Defines the roles of this machine.
CONF_CARTRIDGES="all,-jobsseap" #Not required for a broker but set for consistency.
CONF_INSTALL_METHOD= #Left at default. UNSET. Channel subscriptions are managed via the build.
CONF_DOMAIN="ose-poc.nbnco.internal" #The base domain. Known as the "CLOUD_DOMAIN" in /etc/openshift/broker.conf
CONF_BROKER_HOSTNAME="ose-broker1.ose-poc.nbnco.internal" #Used to set machine name, create DNS entries and throughout entire broker installation.
CONF_NODE_HOSTNAME="ose-node1.ose-poc.nbnco.internal" #Used to create a DNS entry.
CONF_NAMED_HOSTNAME="ns1.ose-poc.nbnco.internal" #Used to create DNS entries and throughout broker installation.
CONF_ACTIVEMQ_HOSTNAME="ose-activemq1.ose-poc.nbnco.internal" #Used to create DNS entries, throughout broker installation and for activemq configuration.
CONF_DATASTORE_HOSTNAME="ose-datastore1.ose-poc.nbnco.internal" # Used to create DNS entries, throughout broker installation and for mongodb configuration.
CONF_BIND_KEY="5Dm/ZGeUGATFKyWjutr2wnllD1OnGM0XHmlwWV6Ybwrfzg+qSgvS+aVkKIF31piURYRJfLpQewisJ8qQYx25tA" # Any base64-encoded value can be used, but ideally an HMAC-SHA256 key generated by dnssec-keygen should be used. This value should be persisted across all broker installs.
CONF_FORWARD_DNS="true" #Creates a forwarders.conf file to allow non-local queries through.
CONF_ACTIVEMQ_REPLICANTS="ose-activemq1.ose-poc.nbnco.internal" #List of ALL members of the activemq HA setup, including itself. This will be used to create activemq network connectors.
CONF_ACTIVEMQ_ADMIN_PASSWORD="password" #An admin password for the activemq console. Useful for troubleshooting.
CONF_ACTIVEMQ_AMQ_USER_PASSWORD="password"  #A password for the amq user activemq uses for secure communications. Must be consistent across all activemq hosts.
CONF_MCOLLECTIVE_PASSWORD="password" #This is the user and password shared between broker and node for communicating over the mcollective topic channels in ActiveMQ. Must be the same on all broker and node hosts.
CONF_MONGODB_BROKER_PASSWORD="password" #The password of the normal user that will be created for the broker to connect to the MongoDB datastore.
CONF_MONGODB_ADMIN_PASSWORD="password" #The password of the administrative user that will be created in the MongoDB datastore.
CONF_DATASTORE_REPLICANTS="ose-datastore1.ose-poc.nbnco.internal:27017" #The full list of ALL mongodb replicas, including itself.
CONF_MONGODB_KEY="password" #  In a replicated setup, this is the key that slaves will use to authenticate with the master. 
CONF_OPENSHIFT_USER1="osepoc" #A basic, locally authenticated user for initial testing.
CONF_OPENSHIFT_PASSWORD1="password" #Password for local user.
CONF_BROKER_AUTH_SALT="1UgoUo2wIc/P/SvVf/ocRhpkqawTsmHH4O6EwQGlALulHuhLrsf88qij7tbTyy9lTO5UmlswunHW65wOfowIWg==" #Random string, common to all hosts, generated with "openssl rand -base64 64"
CONF_BROKER_SESSION_SECRET="5be05a4bf3b9b416204b37614f789778dced2f03476c1445234d5a615a7ceee77f37fb79b91bfd742be104c9a85f4e022ed75d0b94627b0b60ac62dd59fc3def" # Random string, common to all hosts, generated with "openssl rand -hex 64"
CONF_CONSOLE_SESSION_SECRET="b04c4a9e036dda14efca9ea1f77a47e1e2452847cbf55e4a66a4c8249b18752b6a27acee7f1edf2c2e8f94e92b3e94e3b91f909845362b77f928e834c2778121" #Random string, common to all hosts, generated with "openssl rand -hex 64"
CONF_VALID_GEAR_SIZES="small" #Ensuring it's set.

