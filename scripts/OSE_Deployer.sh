############ OSE_Testdrive Deployment Script 
############ Written by Shachar Borenstein <sborenst@redhat.com/shachar.borenstein@gmail.com> 
############ Date 18/10/2014	Version 1.0 
############ Date 16/01/2015	Version 1.2 - Changed IP to be grabbed from cloudformation scripts 
############ Usage: 
############ sh -x OSE_Deployer.sh WORKFLOW TOPDOMAINNAME DEMOUSERPASS
############ Example:
############ Step 01. sh -x  OSE_Deployer.sh CreateConfigini testdrive.tftl.org testdrivepassword1 192.168.0.1,192.168.0.2,192.168.0.2
############ Step 02. sh -x  OSE_Deployer.sh BrokerPreConfig 
############ Step 03. sh -x  OSE_Deployer.sh NodePreConfig node01.testdrive.tftl.org
############ Step 03. sh -x  OSE_Deployer.sh NodePreConfig node02.testdrive.tftl.org
############ Step 04. sh -x  OSE_Deployer.sh BrokerInstall
############ Step 05. sh -x  OSE_Deployer.sh AllNodeInstall all small
############ Step 05. sh -x  OSE_Deployer.sh NodeInstall node01.testdrive.tftl.org php,ruby,postgresql,haproxy,jenkins small
############ Step 05. sh -x  OSE_Deployer.sh NodeInstall node02.testdrive.tftl.org all small
############ Step 05. sh -x  OSE_Deployer.sh BrokerPostConfig 
#
############ Workflows:
############ CreateConfigini - Creates the ose_config.ini file to be used by the rest of the scripts
############ BrokerPreConfig - Runs Broker Pre configuration tasks on the broker (No Parameters required)
############ AllNodePreConfig - Runs Node  Pre configuration tasks on ALL Nodes (No Parameters required)
############ NodePreConfig - Runs Node  Pre configuration tasks on the named node (Requires Node Name as second parameter)
############ BrokerInstall - Runs Broker installer (No Parameters required)
############ AllNodeInstall - Runs install on all nodes (requires Parameters : Cartridges Node_GearSize)
############ NodeInstall runes install pn named gear (requires Parameters : Nodename Cartriges Node_GearSize, i.e : NodeInstall node01.testdrive.tftl.org all small ) 
############ BrokerPostConfig - Run Broker Post configurations (Cartridge install and district creations)
############ 


# Sborenst: I'll add some verification procedure here to make sure the input is valid and present
export NOTIFY_EMAIL="dev@claytonbrown.com";


export WORKFLOW=$1
if [ -f `pwd`/ose_config.ini ]; then
	echo found ose_config.ini `pwd`
	source `pwd`/ose_config.ini 
fi


if [ "$WORKFLOW" == "CreateConfigini" ]; then
	echo starting workflow : $WORKFLOW 
export TOPDOMAINNAME=$2 
export DEMOUSERPASS=$3
export DEFAULTDIR=/root/.OSE_Deployer;
mkdir -p $DEFAULTDIR
touch $DEFAULTDIR/OSE_Deployer.log

export BROKERPUBLICIP=`echo $4 | awk -F, '{print $1}'`    
export NODE01PUBLICIP=`echo $4 | awk -F, '{print $2}'`
export NODE02PUBLICIP=`echo $4 | awk -F, '{print $3}'`

#Variable Definitions 
export MCOLLECTPASS=password1
export MONGODBPASS=password1

export APPSDOMAINNAME=apps.${TOPDOMAINNAME}
export HOSTDOMAINNAME=hosts.${TOPDOMAINNAME}

export BROKERHOSTNAME=broker01.${HOSTDOMAINNAME}
export NODE01HOSTNAME=node01.${HOSTDOMAINNAME}
export NODE02HOSTNAME=node02.${HOSTDOMAINNAME}

# this is the new way, passed by CloudFormations
export BROKERPUBLICIP=`echo $4 | awk -F, '{print $1}'`    
export NODE01PUBLICIP=`echo $4 | awk -F, '{print $2}'`
export NODE02PUBLICIP=`echo $4 | awk -F, '{print $3}'`

## This line is a work around because the broker cannot ssh to itself yet. :( 
##export BROKERDEPLOYNAME=`curl -s http://169.254.169.254/latest/meta-data/public-hostname`    
export BROKERDEPLOYNAME=`ssh root@${BROKERPUBLICIP} "curl -s http://169.254.169.254/latest/meta-data/public-hostname"`    
export NODE01DEPLOYNAME=`ssh root@${NODE01PUBLICIP} "curl -s http://169.254.169.254/latest/meta-data/public-hostname"`    
export NODE02DEPLOYNAME=`ssh root@${NODE02PUBLICIP} "curl -s http://169.254.169.254/latest/meta-data/public-hostname"`    

### sborenst changed from "$NODE01DEPLOYNAME $NODE02DEPLOYNAME" to fix the dns issue.  16/4/15
#export NODELIST="$NODE01DEPLOYNAME $NODE02DEPLOYNAME" 
export NODELIST="$NODE01HOSTNAME $NODE02HOSTNAME" 
#export DISTRICT01MEMBERS=$NODE01DEPLOYNAME,$NODE02DEPLOYNAME
export DISTRICT01MEMBERS=$NODE01HOSTNAME,$NODE02HOSTNAME

export GEARSIZEOPTIONS=small
export GEARSIZEDEFAULT=small

export DISTRICT01NAME=district01_small
export DISTRICT01SIZE=$GEARSIZEDEFAULT

#export DISTRICT02NAME=district02_medium
#export DISTRICT02SIZE=medium

cat << EOF > ose_config.ini
	#################### Variable export into file 
	
	export TOPDOMAINNAME=$TOPDOMAINNAME ; 
	
	export DEMOUSERPASS=$DEMOUSERPASS ; export MCOLLECTPASS=$MCOLLECTPASS ; export MONGODBPASS=$MONGODBPASS ; 
	
	export APPSDOMAINNAME=$APPSDOMAINNAME ; export HOSTDOMAINNAME=$HOSTDOMAINNAME
	
	export BROKERHOSTNAME=$BROKERHOSTNAME ; 
	export NODE01HOSTNAME=$NODE01HOSTNAME ; 
	export NODE02HOSTNAME=$NODE02HOSTNAME ;
	export BROKERDEPLOYNAME=$BROKERDEPLOYNAME export NODE01DEPLOYNAME=$NODE01DEPLOYNAME ; export NODE02DEPLOYNAME=$NODE02DEPLOYNAME ; 
	export NODELIST="$NODELIST" 

	export BROKERPUBLICIP=$BROKERPUBLICIP ; export NODE01PUBLICIP=$NODE01PUBLICIP ; export NODE02PUBLICIP=$NODE02PUBLICIP
	
	export GEARSIZEOPTIONS=$GEARSIZEOPTIONS ; export GEARSIZEDEFAULT=$GEARSIZEDEFAULT
	export DISTRICT01NAME=$DISTRICT01NAME ; export DISTRICT01SIZE=$DISTRICT01SIZE; export DISTRICT01MEMBERS=$DISTRICT01MEMBERS ; 
	export NODEGEARSIZE=$GEARSIZEDEFAULT
	export DEFAULTDIR=$DEFAULTDIR;
	export NOTIFY_EMAIL=$NOTIFY_EMAIL;
EOF
	
fi




# Function BrokerPreConfig
if [ "$WORKFLOW" == "BrokerPreConfig" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started 
	ssh $BROKERDEPLOYNAME "mkdir -p $DEFAULTDIR 2>&1 | tee $DEFAULTDIR/OSE_Deployer.log"
	#ssh $BROKERDEPLOYNAME "wget https://bitbucket.org/sborenst/projectbruce/downloads/Broker_install.sh -O ${DEFAULTDIR}/Broker_install.sh 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
	scp Broker_install.sh $BROKERDEPLOYNAME:$DEFAULTDIR/  2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
	scp ose_config.ini $BROKERDEPLOYNAME:$DEFAULTDIR/ 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
	ssh $BROKERDEPLOYNAME "sh ${DEFAULTDIR}/Broker_install.sh BrokerPreConfig 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
	
fi

# Function AllNodePreConfig
if [ "$WORKFLOW" == "AllNodePreConfig" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
	
	for node in $NODELIST
        do
        	echo XXXXX Running NodePreConfig on node: $node ;
        	ssh $node "mkdir -p $DEFAULTDIR 2>&1| tee $DEFAULTDIR/OSE_Deployer.log"
	#	  	ssh $node "wget https://bitbucket.org/sborenst/projectbruce/downloads/Node_install.sh -O ${DEFAULTDIR}/Node_install.sh 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
			scp Node_install.sh $node:$DEFAULTDIR/ 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
			scp ose_config.ini $node:$DEFAULTDIR/ 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
			ssh $node "sh ${DEFAULTDIR}/Node_install.sh NodePreConfig 2>&1| tee -a $DEFAULTDIR/OSE_Deployer.log"
 		  done; 
touch  $DEFAULTDIR/.debug.${WORKFLOW}.done	
fi

# Function NodePreConfig
if [ "$WORKFLOW" == "NodePreConfig" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
	export node=$2
	     	echo Running NodePreConfig on node: $node ;
        	ssh $node "mkdir -p $DEFAULTDIR 2>&1 | tee $DEFAULTDIR/OSE_Deployer.log"
		  	scp Node_install.sh $node:$DEFAULTDIR/ 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
			scp ose_config.ini $node:$DEFAULTDIR/ 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log
			ssh $node "sh -x ${DEFAULTDIR}/Node_install.sh NodePreConfig 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
fi

# Function BrokerInstall
if [ "$WORKFLOW" == "BrokerInstall" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started 
	ssh $BROKERDEPLOYNAME "sh -x ${DEFAULTDIR}/Broker_install.sh BrokerInstall 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
fi


# Function AllNodeInstall
if [ "$WORKFLOW" == "AllNodeInstall" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
	for node in $NODELIST
        do
        	echo Running NodePreConfig on node: $node ;
			ssh $node "sh -x ${DEFAULTDIR}/Node_install.sh NodeInstall  $node $2 $3 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
 		  done; 
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
fi

# Function NodeInstall
if [ "$WORKFLOW" == "NodeInstall" ]; then
	echo starting workflow : $WORKFLOW
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
	export node=$2
	     	echo Running NodePreConfig on node: $node ;
			ssh $node "sh -x ${DEFAULTDIR}/Node_install.sh NodeInstall  $node $3 $4 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.done	
fi
# Function BrokerPostConfig
if [ "$WORKFLOW" == "BrokerPostConfig" ]; then
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
	echo starting workflow : $WORKFLOW 
	ssh $BROKERDEPLOYNAME "sh -x  ${DEFAULTDIR}/Broker_install.sh BrokerPostConfig 2>&1 | tee -a $DEFAULTDIR/OSE_Deployer.log"
	touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
 fi

