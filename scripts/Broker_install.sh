####################On Broker
####################Be root
# ./Broker_install.sh workflow 

### I need to write the broker scripts to create node scripts, and run a foreach loop for each node in the group
### Nodes should get all of their specific configuration as a parameter 

#################### Variable definition
export WORKFLOW=$1
export NODETYPE=broker
export DEFAULTDIR=/root/.OSE_Deployer;

#################### End Variable definition
 if [ -f $DEFAULTDIR/ose_config.ini ]; then
		source $DEFAULTDIR/ose_config.ini
	else
	 echo $DEFAULTDIR/ose_config.ini not found exiting; exit;
 fi


if [ "$WORKFLOW" == "BrokerPreConfig" ]; then
echo XXXXXXXXXX starting workflow : $WORKFLOW  
touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
# This is to allow Root to ssh itself without question
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

#############dns internal/external hostname issue workaround 16/04/15
echo $NODE01PUBLICIP $NODE01HOSTNAME >> /etc/hosts
echo $NODE02PUBLICIP $NODE02HOSTNAME >> /etc/hosts


# Enable oo-admin-yum-validator repository 
subscription-manager repos --enable rhel-6-server-ose-2.2-infra-rpms
yum install openshift-enterprise-release -y
oo-admin-yum-validator -o 2.2 -r $NODETYPE --fix-all
yum update -y 
curl -O https://raw.githubusercontent.com/openshift/openshift-extras/enterprise-2.2/enterprise/install-scripts/generic/openshift.sh

touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
mail -s "$WORKFLOW completed" $NOTIFY_EMAIL < /dev/null
echo XXXXXXXXXX Completed workflow : $WORKFLOW
 
fi

if [ "$WORKFLOW" == "BrokerInstall" ]; then
echo XXXXXXXXXX starting workflow : $WORKFLOW 
touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
echo sh -x openshift.sh install_components=broker,named,activemq,datastore \
domain=$APPSDOMAINNAME hosts_domain=$HOSTDOMAINNAME \
broker_hostname=$BROKERHOSTNAME \
named_entries=broker01:$BROKERPUBLICIP,activemq:$BROKERPUBLICIP,node01:$NODE01PUBLICIP,node02:$NODE02PUBLICIP \
valid_gear_sizes=$GEARSIZEOPTIONS default_gear_size=$GEARSIZEDEFAULT default_gear_capabilities=$GEARSIZEDEFAULT \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log

sh -x openshift.sh install_components=broker,named,activemq,datastore \
domain=$APPSDOMAINNAME hosts_domain=$HOSTDOMAINNAME \
broker_hostname=$BROKERHOSTNAME \
named_entries=broker01:$BROKERPUBLICIP,activemq:$BROKERPUBLICIP,node01:$NODE01PUBLICIP,node02:$NODE02PUBLICIP \
valid_gear_sizes=$GEARSIZEOPTIONS default_gear_size=$GEARSIZEDEFAULT default_gear_capabilities=$GEARSIZEDEFAULT \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log

touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
mail -s "$WORKFLOW completed" $NOTIFY_EMAIL < /dev/null
echo XXXXXXXXXX Completed workflow : $WORKFLOW
fi

if [ "$WORKFLOW" == "BrokerPostConfig" ]; then
echo XXXXXXXXXX starting workflow : $WORKFLOW 
touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
### This is the post install stuff, should only run all nodes are fully installed and configured
 oo-admin-ctl-district -c create -n $DISTRICT01NAME -p $DISTRICT01SIZE
 oo-admin-ctl-district -c add-node -n $DISTRICT01NAME -i $DISTRICT01MEMBERS
 oo-admin-ctl-cartridge -c import-node --activate --obsolete
 oo-admin-ctl-cartridge -c import-node --activate --obsolete --node $NODE01HOSTNAME #--dry-run
 oo-admin-ctl-cartridge -c import-node --activate --obsolete --node $NODE02HOSTNAME
 
 oo-admin-ctl-cartridge -c clean
 oo-admin-broker-cache --clear # --console
 oo-admin-console-cache --clear
 oo-admin-ctl-cartridge -c list


touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
mail -a /var/log/user-data.log -s "$WORKFLOW completed" $NOTIFY_EMAIL < /dev/null
echo XXXXXXXXXX Completed workflow : $WORKFLOW
fi 
