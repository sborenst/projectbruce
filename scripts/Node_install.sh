####################On Node
####################Be root
# ./node0x_install.sh NodePreConfig|NodeInstall|installcartridges [node01.hosts.testdrive.tftl.org] [all|php,ruby,postgresql,haproxy,jenkins] [small]  
#################### Variable definition
export WORKFLOW=$1
export WHATNODEAMI=$2 
export CARTRIDGEOPS=$3
export NODEGEARSIZE=$4
export NODETYPE=node-eap
export DEFAULTDIR=/root/.OSE_Deployer;
export DEFAULTDIR=/root/.OSE_Deployer;
mkdir -p $DEFAULTDIR
touch $DEFAULTDIR/OSE_Deployer.log
#################### End Variable definition


 if [ -f $DEFAULTDIR/ose_config.ini ]; then
		source $DEFAULTDIR/ose_config.ini
	else
	 echo $DEFAULTDIR/ose_config.ini not found exiting; exit;
 fi


if [ "$WORKFLOW" == "NodePreConfig" ]; then
echo XXXXXXXXXX starting workflow : $WORKFLOW 
touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
subscription-manager repos --enable rhel-6-server-ose-2.2-node-rpms
yum install openshift-enterprise-release -y
oo-admin-yum-validator -o 2.2 -r $NODETYPE --fix-all
## This is a workaround for the multilib bug. should be fixed in the next few days
#yum install libcgroup-pam --enablerepo=rhel-6-server-optional-rpms --disableplugin=priorities -y
yum update -y 
curl -O https://raw.githubusercontent.com/openshift/openshift-extras/enterprise-2.2/enterprise/install-scripts/generic/openshift.sh

mail -s "$WORKFLOW completed" $NOTIFY_EMAIL < /dev/null
touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
echo XXXXXXXXXX Completed workflow : $WORKFLOW
fi

if [ "$WORKFLOW" == "NodeInstall" ]; then
echo XXXXXXXXXX starting workflow : $WORKFLOW 
touch  $DEFAULTDIR/.debug.${WORKFLOW}.started
## The script will run this command 
echo sh openshift.sh install_components=node domain=$APPSDOMAINNAME \
hosts_domain=$HOSTDOMAINNAME node_hostname=$WHATNODEAMI \
named_ip_addr=$BROKERPUBLICIP broker_ip_addr=$BROKERPUBLICIP broker_hostname=$BROKERHOSTNAME  \
node_profile=$NODEGEARSIZE cartridges=$CARTRIDGEOPS \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log


sh openshift.sh install_components=node domain=$APPSDOMAINNAME \
hosts_domain=$HOSTDOMAINNAME node_hostname=$WHATNODEAMI \
named_ip_addr=$BROKERPUBLICIP broker_ip_addr=$BROKERPUBLICIP broker_hostname=$BROKERHOSTNAME  \
node_profile=$NODEGEARSIZE cartridges=$CARTRIDGEOPS \
mcollective_password=$MCOLLECTPASS mongodb_broker_password=$MONGODBPASS openshift_password1=$DEMOUSERPASS \
install_method=none 2>&1 | tee -a openshift.sh.log

touch  $DEFAULTDIR/.debug.${WORKFLOW}.done
mail -s "$WORKFLOW completed" $NOTIFY_EMAIL < /dev/null
echo XXXXXXXXXX Completed workflow : $WORKFLOW
echo XXXXXXXXXX $WORKFLOW XXXXX REBOOTING XXXXX
reboot
fi
