{

  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "AWS CloudFormation barebones for OSE TestDrive",

  "Parameters" : {

    "InstanceType" : {
      "Description" : "OSE Broker EC2 instance type",
      "Type" : "String",
      "AllowedValues" : [
        "t2.micro",
        "t2.small",
        "t2.medium"
      ],
      "Default" : "t2.medium",
      "ConstraintDescription" : "must be a valid EC2 instance type."
    },
     "RHNUSER" : {
    "NoEcho" : "false",
    "Description" : "The rhn account name",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "41",
    "AllowedPattern" : ".*"
  },
     "RHNPASS" : {
    "NoEcho" : "true",
    "Description" : "The rhn account password",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "41",
    "AllowedPattern" : ".*"
  },
   "RHNPOOL" : {
    "NoEcho" : "false",
    "Description" : "The rhn pool",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "41",
    "AllowedPattern" : ".*"
  },
    "BASEDOMAIN" : {
    "NoEcho" : "false",
    "Description" : "The Base DNS Domain",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "60",
    "AllowedPattern" : ".*"
  },
    "HostedZoneId" : {
    "NoEcho" : "false",
    "Description" : "The Base DNS Domain",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "60",
    "AllowedPattern" : ".*"
  },
  "DEMOUSERPASSWORD" : {
    "NoEcho" : "yes",
    "Description" : "Password for the Demo user on the deployed OSE",
    "Type" : "String",
    "MinLength" : "1",
    "MaxLength" : "60",
    "AllowedPattern" : ".*"
  },
  "WorkerCapacity": {
      "Default": "2",
      "Description" : "The initial number of worker instances",
      "Type": "Number",
      "MinValue": "1",
      "MaxValue": "5",
      "ConstraintDescription" : "must be between 1 and 5 worker instances."
    },

    "KeyName" : {
      "Default" : "OpenshiftTestdrive",
      "Type" : "String",
      "MaxLength" : "255",
      "MinLength" : "1",
      "AllowedPattern" : "[\\x20-\\x7E]*",
      "Description" : "Name of an existing EC2 KeyPair to enable SSH access to the instance",
      "ConstraintDescription" : "can contain only ASCII characters."
    },

    "SSHLocation" : {
      "Default" : "0.0.0.0/0",
      "Type" : "String",
      "MaxLength" : "18",
      "MinLength" : "9",
      "AllowedPattern" : "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "Description" : " The IP address range that can be used to SSH to the EC2 instances",
      "ConstraintDescription" : "must be a valid IP CIDR range of the form x.x.x.x/x."
    }
  },


  "Mappings" : {
    "AWSRegionArch2AMI" : {
      "ap-southeast-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "sa-east-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "eu-west-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "ap-southeast-2" : {
        "64PVM" : "ami-b72fb78d",
        "64HVM" : "ami-452eb67f"
      },
      "us-east-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "us-west-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "ap-northeast-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "us-gov-west-1" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      },
      "us-west-2" : {
        "64PVM" : "ami-TodoTodo",
        "64HVM" : "ami-TodoTodo"
      }
    },

    "AWSInstanceType2Arch" : {
      "t2.medium" 	: {"Arch" : "64HVM"},
      "t2.micro" 	: {"Arch" : "64HVM"},
      "t2.small" 	: {"Arch" : "64HVM"}
    }
  },


  "Resources" : {

    "NetworkAcl" : {
      "Type" : "AWS::EC2::NetworkAcl",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "Tags" : [{"Key" : "Application","Value" : {"Ref" : "AWS::StackId"}}]
      }
    },
    "BrokerInstanceDNSRecord" : {
        "Type" : "AWS::Route53::RecordSet",
        "Properties" : {
            "HostedZoneId" : {"Ref" : "HostedZoneId"},
            "Comment" : "A records for my frontends.",
            "Name" : {"Fn::Join" : [".",[ "broker",{"Ref" : "VPC"},{"Ref" : "BASEDOMAIN"}]]},
            "Type" : "A",
            "TTL" : "2",
            "ResourceRecords" : [
        { "Fn::GetAtt" : [ "BrokerInstance", "PublicIp" ] }
            ]
        }
    },
   	"HostsDomainDNSRecord" : {
        "Type" : "AWS::Route53::RecordSet",
        "Properties" : {
            "HostedZoneId" : {"Ref" : "HostedZoneId"},
            "Comment" : "A records for my frontends.",
            "Name" : {"Fn::Join" : [".",[ "hosts",{"Ref" : "VPC"},{"Ref" : "BASEDOMAIN"}]]},
            "Type" : "NS",
            "TTL" : "2",
            "ResourceRecords" : [
        { "Fn::GetAtt" : [ "BrokerInstance", "PublicIp" ] }
            ]
        }
    },

   	"AppsDomainDNSRecord" : {
        "Type" : "AWS::Route53::RecordSet",
        "Properties" : {
            "HostedZoneId" : {"Ref" : "HostedZoneId"},
            "Comment" : "A records for my frontends.",
            "Name" : {"Fn::Join" : [".",[ "apps",{"Ref" : "VPC"},{"Ref" : "BASEDOMAIN"}]]},
            "Type" : "NS",
            "TTL" : "2",
            "ResourceRecords" : [
        { "Fn::GetAtt" : [ "BrokerInstance", "PublicIp" ] }
            ]
        }
   	},

    "AttachGateway" : {
      "Type" : "AWS::EC2::VPCGatewayAttachment",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "InternetGatewayId" : {"Ref" : "InternetGateway"}
      }
    },

    "S3ConfigBucket" : {
      "DeletionPolicy" : "Retain",
      "Type" : "AWS::S3::Bucket",
      "Properties" : {
        "AccessControl" : "PublicReadWrite",
        "WebsiteConfiguration" : {
          "IndexDocument" : "index.html",
          "ErrorDocument" : "error.html"
        }
      }
    },

    "S3AccessProfile" : {
      "Type" : "AWS::IAM::InstanceProfile",
      "Properties" : {
        "Roles" : [{"Ref" : "S3AccessRole"}],
        "Path" : "/"
      }
    },

    "InboundResponsePortsNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "false",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "102",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "1024","To" : "65535"}
      }
    },

    "S3AccessRole" : {
      "Type" : "AWS::IAM::Role",
      "Properties" : {
        "AssumeRolePolicyDocument" : {
          "Version" : "2012-10-17",
          "Statement" : [
            {
              "Principal" : {"Service" : ["ec2.amazonaws.com"]},
              "Action" : ["sts:AssumeRole"],
              "Effect" : "Allow"
            }
          ]
        },
        "Path" : "/",
        "Policies" : [
          {
            "PolicyName" : "root",
            "PolicyDocument" : {
              "Version" : "2012-10-17",
              "Statement" : [
                {
                  "Action" : [
                    "s3:GetBucketLocation",
                    "s3:ListBucket"
                  ],
                  "Resource" : [{"Fn::Join" : ["",["arn:aws:s3:::",{"Ref" : "S3ConfigBucket"}]]}],
                  "Effect" : "Allow"
                },
                {
                  "Action" : [
                    "s3:GetObject",
                    "s3:PutObject",
                    "s3:PutObjectACL"
                  ],
                  "Resource" : [{"Fn::Join" : ["",["arn:aws:s3:::",{"Ref" : "S3ConfigBucket"},"/*"]]}],
                  "Effect" : "Allow"
                }
              ]
            }
          }
        ]
      }
    },
    "SubnetRouteTableAssociationA" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "RouteTableId" : {"Ref" : "RouteTable"},
        "SubnetId" : {"Ref" : "SubnetA"}
      }
    },
    "SubnetRouteTableAssociationB" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "RouteTableId" : {"Ref" : "RouteTable"},
        "SubnetId" : {"Ref" : "SubnetB"}
      }
    },

    "WorkerWaitCondition" : {
      "Properties" : {
        "Timeout" : "400",
        "Handle" : {"Ref" : "WorkerWaitHandle"},
        "Count" : "1"
      },
      "Type" : "AWS::CloudFormation::WaitCondition",
      "DependsOn" : "BrokerInstance"
    },

    "InternetGateway" : {
      "Type" : "AWS::EC2::InternetGateway",
      "Properties" : {
        "Tags" : [{"Key" : "Application","Value" : {"Ref" : "AWS::StackId"}}]
      }
    },

    "BrokerInstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "GroupDescription" : "Enable SSH access via port 22",
        "SecurityGroupIngress" : [
          {
            "CidrIp" : {"Ref" : "SSHLocation"},
            "FromPort" : "22",
            "ToPort" : "22",
            "IpProtocol" : "tcp"
          },
          {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "80",
            "ToPort" : "80",
            "IpProtocol" : "tcp"
          },
           {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "53",
            "ToPort" : "53",
            "IpProtocol" : "udp"
          },
            {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "61613",
            "ToPort" : "61613",
            "IpProtocol" : "tcp"
          },
            {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "443",
            "ToPort" : "443",
            "IpProtocol" : "tcp"
          }
        ]
      }
    },


    "OutBoundHTTPNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "true",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "100",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "80","To" : "80"}
      }
    },


    "VPC" : {
      "Type" : "AWS::EC2::VPC",
      "Properties" : {
        "CidrBlock" : "10.0.0.0/16",
        "EnableDnsSupport" : "true",
        "EnableDnsHostnames" : "true",
        "Tags" : [
          {
            "Key" : "Application",
            "Value" : {"Ref" : "AWS::StackId"}
          }
        ]
      }
    },
    "Route" : {
      "Properties" : {
        "DestinationCidrBlock" : "0.0.0.0/0",
        "GatewayId" : {"Ref" : "InternetGateway"},
        "RouteTableId" : {"Ref" : "RouteTable"}
      },
      "Type" : "AWS::EC2::Route",
      "DependsOn" : "AttachGateway"
    },
    "InboundHTTPNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "false",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "100",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "80","To" : "80"}
      }
    },
    "OutBoundHTTPSNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "true",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "101",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "443","To" : "443"}
      }
    },
    "OutBoundResponsePortsNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "true",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "102",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "1024","To" : "65535"}
      }
    },
    "RouteTable" : {
      "Type" : "AWS::EC2::RouteTable",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "Tags" : [{"Key" : "Application","Value" : {"Ref" : "AWS::StackId"}}]
      }
    },

    "WorkerWaitHandle" : {
      "Type" : "AWS::CloudFormation::WaitConditionHandle"
    },

    "SubnetA" : {
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "AvailabilityZone" : {"Fn::Select" : [ "0", { "Fn::GetAZs" : { "Ref" : "AWS::Region" }}]},
        "CidrBlock" : "10.0.0.0/24",
        "Tags" : [{"Key" : "Application","Value" : {"Ref" : "AWS::StackId"}}]
      }
    },

    "SubnetB" : {
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "AvailabilityZone" : {"Fn::Select" : [ "1", { "Fn::GetAZs" : { "Ref" : "AWS::Region" }}]},
        "CidrBlock" : "10.0.1.0/24",
        "Tags" : [{"Key" : "Application","Value" : {"Ref" : "AWS::StackId"}}]
      }
    },

    "WorkerInstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "GroupDescription" : "Enable SSH access via port 22",
        "SecurityGroupIngress" : [
          {
            "CidrIp" : {"Ref" : "SSHLocation"},
            "FromPort" : "22",
            "ToPort" : "22",
            "IpProtocol" : "tcp"
          },
            {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "443",
            "ToPort" : "443",
            "IpProtocol" : "tcp"
          },
          {
            "CidrIp" : "0.0.0.0/0",
            "FromPort" : "80",
            "ToPort" : "80",
            "IpProtocol" : "tcp"
          }
        ]
      }
    },
    "InboundSSHNetworkAclEntry" : {
      "Type" : "AWS::EC2::NetworkAclEntry",
      "Properties" : {
        "Egress" : "false",
        "NetworkAclId" : {"Ref" : "NetworkAcl"},
        "RuleNumber" : "101",
        "RuleAction" : "allow",
        "CidrBlock" : "0.0.0.0/0",
        "Protocol" : "6",
        "PortRange" : {"From" : "22","To" : "22"}
      }
    },


    "BrokerInstance" : {
      "Type" : "AWS::EC2::Instance",
      "Metadata" : {
        "Comment" : "Installation scipt",
        "AWS::CloudFormation::Init" : {
          "config" : {
            "files" : {
              "/etc/cfn/hooks.d/cfn-auto-reloader.conf" : {
                "content" : {
                  "Fn::Join" : ["",
                    [
                      "[cfn-auto-reloader-hook]\n",
                      "triggers=post.update\n",
                      "path=Resources.BrokerInstance.Metadata.AWS::CloudFormation::Init\n",
                      "action=/usr/bin/cfn-init -s ",{"Ref" : "AWS::StackId"}," -r BrokerInstance "," --region ",{"Ref" : "AWS::Region"},"\n","runas=root\n"
                    ]
                  ]
                }
              },
              "/etc/cfn/cfn-hup.conf" : {
                "group" : "root",
                "content" : {"Fn::Join" : ["",["[main]\n","stack=",{"Ref" : "AWS::StackId"},"\n","region=",{"Ref" : "AWS::Region"},"\n"]]},
                "owner" : "root",
                "mode" : "000400"
              }
            },
            "services" : {
              "sysvinit" : {
                "httpd" : {
                  "enabled" : "true",
                  "ensureRunning" : "true"
                }
              }
            },
            "packages" : {
              "yum" : {"httpd" : []}
            }
          }
        }
      },

      "Properties" : {
        "UserData" : {
          "Fn::Base64" : {
            "Fn::Join" : [
              "",
              [
                "#!/bin/bash\n",
                "#cfn-signal Helper function\n",
                "function error_exit\n",
                "{\n",
                "  /usr/bin/cfn-signal -e 1 -r \"$1\" '",{"Ref" : "WorkerWaitHandle"},"'\n",
                "  exit 1\n",
                "}\n",

                "#Lay down some basic dependencies\n",
                "easy_install pip\n",
                "yum reinstall python-requests\n",
                "pip install awscli --upgrade\n",
                "pip install python-daemon\n",
                "complete -C aws_completer aws\n",
                "export AWS_DEFAULT_REGION=",{"Ref" : "AWS::Region"},"\n",

                "#Install CFN Tools\n",
                "wget https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz\n",
                "tar -zxf aws-cfn-bootstrap-latest.tar.gz\n",
                "cd aws-cfn-bootstrap-* && python setup.py install\n",
                "/usr/bin/cfn-init -s ",{"Ref" : "AWS::StackId"}," -r BrokerInstance  --region ",{"Ref" : "AWS::Region"}," || error_exit 'Failed to run cfn-init'\n",

                "# Generate Public PrivateKeyPair\n",
                "ssh-keygen -f  /root/.ssh/id_rsa -t rsa -N ''\n",
                "echo Generated Public / Private Key Pair\n",
                "aws s3 cp /root/.ssh/id_rsa.pub s3://",{"Ref" : "S3ConfigBucket"},"  --acl authenticated-read\n",
                "cp ./id_rsa.pub /root/.ssh/\n",
                "chmod 644 /root/.ssh/id_rsa.pub\n",
                "echo Deployed private key to Ec2 User\n",
                "cat /root/.ssh/id_rsa\n",
                "echo --------Public Key--------\n",
                "cat /root/.ssh/id_rsa.pub\n",

                "# Signal the workers to launch\n",
                "/usr/bin/cfn-signal -e 0 -r \"Key generation complete in s3\" '",{"Ref" : "WorkerWaitHandle"},"'\n",

                "# Start up the cfn-hup daemon to listen for changes to the metadata\n",
                "/usr/bin/cfn-hup || error_exit 'Failed to start cfn-hup'\n",

                "#Lay down some PaaS goodies\n",
                "#Do stuff\n",
                "#/usr/bin/cfn-signal -e 0 -r \"Broker setup complete\" '",{"Ref" : "WorkerWaitHandle"},"'\n",

                "#Broker Boostrap goes here\n",
                "#TODO TODO\n",
                "echo StrictHostKeyChecking no >> /etc/ssh/ssh_config \n",
					 "rm -f /etc/yum.repos.d/redhat-rhui* \n",
					 "subscription-manager register --username=",{"Ref" : "RHNUSER"}," --password=",{"Ref" : "RHNPASS"},"\n",
					 "subscription-manager attach --pool ",{"Ref" : "RHNPOOL"},"\n",
					 "subscription-manager repos --disable=* \n",
					 "subscription-manager repos --enable rhel-6-server-rpms \n",
                "echo sudo bash >> ~ec2-user/.bashrc\n",
                "wget https://bitbucket.org/sborenst/projectbruce/get/master.zip \n",
                "unzip master.zip \n",
                "cp */scripts/OSE_Deployer.sh . \n",
                "cp */scripts/Node_install.sh . \n",
                "cp */scripts/Broker_install.sh . \n",

                "#Create JSON array of workers \n",
                "sleep 2m\n",
               	"aws ec2 describe-instances --filter Name=tag:Name,Values=WorkerNode Name=vpc-id,Values=", {"Ref" : "VPC"}," --query 'Reservations[].Instances[].[InstanceId,PublicDnsName,PublicIpAddress,PrivateDnsName,SubnetId,PrivateIpAddress]' > workers.json\n",

                "#TODO TODO: iterate JSON array and prepare workers\n",
                "#TODO TODO: optional we can start a daemon to poll the queue indefinately and register/prepare new workers (Event: autoscaling:EC2_INSTANCE_LAUNCH)\n",

				    "sh -x OSE_Deployer.sh CreateConfigini ",{"Ref" : "BASEDOMAIN"}," ",{"Ref" : "DEMOUSERPASSWORD"}," 54.206.41.116,54.66.146.226,54.206.76.185 \n",
				    "sh -x OSE_Deployer.sh AllNodePreConfig \n",
				    "sh -x OSE_Deployer.sh BrokerPreConfig \n",
           		 "# All done so signal success\n",


                "#/usr/bin/cfn-signal -e 0 -r \"Broker setup complete\" '",{"Ref" : "WorkerWaitHandle"},"'\n"
              ]
            ]
          }
        },
        "KeyName" : {"Ref" : "KeyName"},
        "IamInstanceProfile" : {"Ref" : "S3AccessProfile"},
        "ImageId" : {"Fn::FindInMap" : ["AWSRegionArch2AMI",{"Ref" : "AWS::Region"},"64HVM"]},
        "NetworkInterfaces" : [
          {
            "DeleteOnTermination" : "true",
            "GroupSet" : [{"Ref" : "BrokerInstanceSecurityGroup"}],
            "DeviceIndex" : "0",
            "AssociatePublicIpAddress" : "true",
            "SubnetId" : {"Ref" : "SubnetA"}
          }
        ],
        "InstanceType" : {"Ref" : "InstanceType"},
        "Tags" : [{"Value" : "BrokerNode","Key" : "Name"}]
      }
    },

    "WorkerStartQueue" : {
      "Type" : "AWS::SQS::Queue",
      "Properties" : {
         "QueueName" : "WorkerStartQueue"
      }
    },

    "NotificationTopic": {
      "Type": "AWS::SNS::Topic",
      "DependsOn" : "WorkerStartQueue",
      "Properties": {
        "Subscription": [ 	{"Endpoint": "clbrown@amazon.com" ,"Protocol": "email" },
          					{"Endpoint":{"Fn::GetAtt":["WorkerStartQueue","Arn"]} ,"Protocol": "sqs" }
                        ]
      }
    },


    "WorkerAutoScalingGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "DependsOn" : "WorkerWaitCondition",
      "Properties" : {
        "AvailabilityZones" : { "Fn::GetAZs" : "" },
        "VPCZoneIdentifier" : [ {"Ref" : "SubnetA"}, {"Ref" : "SubnetB"}],
        "LaunchConfigurationName" : { "Ref" : "WorkerLaunchConfig" },
        "Tags" : [{"Key" : "Name", "Value" : "WorkerNode","PropagateAtLaunch" : "true" }],
        "MinSize" : "1",
        "MaxSize" : "5",
        "DesiredCapacity" : { "Ref" : "WorkerCapacity" },
        "NotificationConfiguration" : {
          "TopicARN" : { "Ref" : "NotificationTopic" },
          "NotificationTypes" : [ "autoscaling:EC2_INSTANCE_LAUNCH","autoscaling:EC2_INSTANCE_LAUNCH_ERROR","autoscaling:EC2_INSTANCE_TERMINATE", "autoscaling:EC2_INSTANCE_TERMINATE_ERROR"]
        }
      }
    },

    "WorkerLaunchConfig": {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "DependsOn" : "WorkerInstanceSecurityGroup",
      "Metadata" : {
          "Comment" : "Worker Installation scipt",
          "AWS::CloudFormation::Init" : {
            "config" : {
              "files" : {
                "/etc/cfn/hooks.d/cfn-auto-reloader.conf" : {
                  "content" : {
                    "Fn::Join" : [
                      "",
                      [
                        "[cfn-auto-reloader-hook]\n",
                        "triggers=post.update\n",
                        "path=Resources.BrokerInstance.Metadata.AWS::CloudFormation::Init\n",
                        "action=/usr/bin/cfn-init -s ",{"Ref" : "AWS::StackId"}," -r WorkerLaunchConfig "," --region ",{"Ref" : "AWS::Region"},"\n","runas=root\n"
                      ]
                    ]
                  }
                },
                "/etc/cfn/cfn-hup.conf" : {
                  "group" : "root",
                  "content" : {"Fn::Join" : ["",["[main]\n","stack=",{"Ref" : "AWS::StackId"},"\n","region=",{"Ref" : "AWS::Region"},"\n"]]},
                  "owner" : "root",
                  "mode" : "000400"
                }
              },
              "services" : {"sysvinit" : {}},
              "packages" : {"yum" : {}}
            }
          }
      },

      "Properties": {
        "KeyName" : {"Ref" : "KeyName"},
        "IamInstanceProfile" : {"Ref" : "S3AccessProfile"},
        "AssociatePublicIpAddress" : "true",

        "ImageId" : {"Fn::FindInMap" : ["AWSRegionArch2AMI",{"Ref" : "AWS::Region"},"64HVM"]},
        "InstanceType" : {"Ref" : "InstanceType"},
        "SecurityGroups" : [ {"Ref" : "WorkerInstanceSecurityGroup"} ],
        "UserData" : {
          "Fn::Base64" : {
            "Fn::Join" : [
              "",
              [
                "#!/bin/bash\n",
                "#cfn-signal Helper function\n",
                "function error_exit\n",
                "{\n",
                	"/usr/bin/cfn-signal -e 1 -r \"$1\" '",{"Ref" : "WorkerWaitHandle"},"'\n","  exit 1\n",
                "}\n",

                "#Lay down some basic dependencies\n",
                "easy_install pip\n",
                "yum reinstall python-requests\n",
                "pip install awscli --upgrade\n",
                "pip install python-daemon\n",
                "complete -C aws_completer aws\n",

                "#Install public key from s3\n",
                "aws s3 cp  s3://",{"Ref" : "S3ConfigBucket"},"/id_rsa.pub ./broker_id_rsa.pub\n",
                "cat ./broker_id_rsa.pub >> /root/.ssh/authorized_keys\n",
                "chmod 700 ~/.ssh\n",
                "chmod 600 ~/.ssh/authorized_keys\n",
                "echo Added Broker Key to authorized_keys\n",
                "cat /root/.ssh/authorized_keys\n",
                "sed -i 's/#PermitRootLogin.*/PermitRootLogin yes/g' /etc/ssh/sshd_config\n",
                "sed -i 's/#PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config\n",

					 "echo StrictHostKeyChecking no >> /etc/ssh/ssh_config \n",
					 "rm -f /etc/yum.repos.d/redhat-rhui* \n",
					 "subscription-manager register --username=",{"Ref" : "RHNUSER"}," --password=",{"Ref" : "RHNPASS"},"\n",
					 "subscription-manager attach --pool ",{"Ref" : "RHNPOOL"},"\n",
					 "subscription-manager repos --disable=* \n",
					 "subscription-manager repos --enable rhel-6-server-rpms \n",
                "echo sudo bash >> ~ec2-user/.bashrc\n",


                "#Install CFN Tools\n",
                "wget https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz\n",
                "tar -zxf aws-cfn-bootstrap-latest.tar.gz\n",
                "cd aws-cfn-bootstrap-* && python setup.py install\n",
                "/usr/bin/cfn-init -s ",{"Ref" : "AWS::StackId"}," -r WorkerLaunchConfig "," --region ",{"Ref" : "AWS::Region"}," || error_exit 'Failed to run cfn-init on worker'\n",

                "# Start up the cfn-hup daemon to listen for changes to the Web Server metadata\n",
                "/usr/bin/cfn-hup || error_exit 'Failed to start cfn-hup'\n",

                "#Any Worker Boostrap goes here\n",
                "#TODO TODO\n",

                "# All done so signal success\n",
                "/usr/bin/cfn-signal -e 0 -r \"Workers setup complete\" '",
                {
                  "Ref" : "WorkerWaitHandle"
                },
                "'\n"
              ]
            ]
          }
      }}
    }
  },
  "Outputs" : {
    "URL" : {
      "Value" : {"Fn::Join" : ["",["http://",{"Fn::GetAtt" : ["BrokerInstance","PublicIp"]}]]},
      "Description" : "Newly created application URL"
    },
    "S3WebsiteURL" : {
      "Value" : {"Fn::GetAtt" : ["S3ConfigBucket","WebsiteURL"]},
      "Description" : "URL for website hosted on S3"
    },
    "S3BucketSecureURL" : {
      "Value" : {"Fn::Join" : ["",["https://",{"Fn::GetAtt" : ["S3ConfigBucket","DomainName"]}]]},
      "Description" : "Name of S3 bucket to hold website content"
    },
    "BrokerInstance" : {"Value" : {"Fn::GetAtt" : ["BrokerInstance","PublicIp"]}}

	}
}
