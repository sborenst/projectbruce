import boto.ec2
import optparse
import time


def getInstanceIPs(region, vpcid, tagName=u'WorkerNode', fleetsize=1, debug=False):
    instanceIPs = set()
    brokerNodeIp = None
    # print region
    # conn = boto.ec2.connect_to_region(region)
    conn = boto.ec2.connect_to_region(
        'ap-southeast-2', aws_access_key_id="AKIAJA4INYZXMVBBU3KA", aws_secret_access_key="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")
    # print "conn: %s " % conn

    # print reservations
    loops = 0
    vpcFilter = {"vpc-id": vpcid}

    while len(instanceIPs) < fleetsize and loops < 100:
        if debug:
            print 'scanning for reservations in %s' % vpcFilter

        reservations = conn.get_all_instances(filters=vpcFilter)
        for res in reservations:
            for inst in res.instances:
                if u'Name' in inst.tags:
                    if unicode('BrokerNode') in inst.tags[u'Name'] and brokerNodeIp is None:
                        brokerNodeIp = inst.ip_address
                        if debug:
                            print "Broker: %s" % (brokerNodeIp)

                    if unicode(tagName) in inst.tags[u'Name']:
                        if debug:
                            print 'Worker: %s' % (inst.ip_address)
                        if inst.ip_address is not None:
                            if inst.ip_address not in instanceIPs:
                                instanceIPs.add(inst.ip_address)
                else:
                    if debug:
                        print 'instance has no name tag %s' % (inst)
        time.sleep(5)
        loops += 1
    if debug:
        print "Broker: %s" % brokerNodeIp
        print "Workers: %s" % instanceIPs
    return brokerNodeIp + ',' + ','.join(instanceIPs)

if __name__ == "__main__":
    parser = optparse.OptionParser(
        description="Scan EC2 Pool for EC2 instance Tagged with a specific tag")
    parser.add_option(
        '--region',  metavar='region', help='aws region', default='ap-southeast-2')
    parser.add_option(
        '--vpcid',   metavar='vpc', help='aws vpc-id name', default='All')
    parser.add_option('--tagname', metavar='tagname',
                      help='the name tag used to determine fleet members')
    parser.add_option('--fleetsize', metavar='fleetsize',
                      type=int,  help='an integer for the expected fleet size')
    parser.add_option('--debug', help='print additional debug', action="store_true")
    args = parser.parse_args()[0]
    if not(args.tagname and args.region and args.vpcid and args.fleetsize):
        print parser.print_help()
    else:
        # print args.tagname, args.region, args.vpcid, args.fleetsize
        ips = getInstanceIPs(tagName=args.tagname, region=args.region, fleetsize=args.fleetsize, vpcid=args.vpcid, debug=args.debug)
        f = open('./result-ec2-ips.json', 'wb')
        f.write(ips)
        f.close()
        print ips
